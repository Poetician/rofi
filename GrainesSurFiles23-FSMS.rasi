/**
 * Base16 oomox-0018 Round Suru ROFI Color theme
 *
 * Authors
 *  Scheme: GrainesSurFiles Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    clear:                       #00000000;
    select:                      #62310899;
    urgent:                      #c74506;
    brown1:                      #7e3407;
    brown2:                      #360800;
    cream:                       #fecb8a;
    grey:                        #6c676180;
    lightfg:                     #ffffe3;
    lightbg:                     #795b314d;
    foreground:                  #ffdf75;
    background:                  #1c20201a;
    background-color:            #1c202033;
    separator-color:             @foreground;
    border-color:                @foreground;
    selected-normal-foreground:  @lightfg;
    selected-normal-background:  @select;
    selected-active-foreground:  @background;
    selected-active-background:  @grey;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @urgent;
    normal-foreground:           @foreground;
    normal-background:           @background;
    active-foreground:           @grey;
    active-background:           @background;
    urgent-foreground:           @urgent;
    urgent-background:           @select;
    alternate-normal-foreground: @foreground;
    alternate-normal-background: @brown1;
    alternate-active-foreground: @grey;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @urgent;
    alternate-urgent-background: @select;
    spacing:                     2;
}
window {
    fullscreen:       true ;
    background-image: url("graines.jpg") ;
    border:           6 3 4 3 ;
    border-radius:    12px ;
    padding:          15 15 17 ;
}
mainbox {
    background-color: @clear ;
    border:           5 2 3 2 ;
    border-radius:    10 ;
    margin:           15 15 17 ;
    padding:          15 ;
}
inputbar {
    background-color: @clear;
    border:           4px 1px 2px 1px ;
    border-color:     @foreground;
    border-radius:    10% ;
    margin:           17 300 17 ;
    padding:          6px 10px 6px 10px ;
    spacing:          5 ;
    text-color:       @normal-foreground;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    spacing:          0;
    text-color:       @normal-foreground;
    background-color: @clear;
}
entry {
    spacing:          0;
    text-color:       @normal-foreground;
    background-color: @clear;
}
prompt {
    spacing:          0;
    text-color:       @normal-foreground;
    background-color: @clear;
}
textbox-prompt-colon {
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
    background-color: @clear;
}
message {
    border:           4px 1px 2px 1px ;
    border-color:     @foreground;
    border-radius:    10% ;
    margin:           8 200 ;
    padding:          10 15 ;
}
textbox {
    border:           4px 2px 3px 2px ;
    border-color:     @foreground;
    border-radius:    10% ;
    margin:           2 ;
    padding:          10 ;
    text-color:       @foreground;
}
element-icon {
    size:             5 em ;
}
element {
    orientation:      horizontal;
}
element-text {
    vertical-align:   0.450;
    text-color:       inherit;
}
listview {
    columns:          2;
    background-color: @clear ;
    border:           5px 2px 3px 2px ;
    border-color:     @foreground;
    border-radius:    20 ;
    margin:           15 20 ;
    padding:          10px 15px ;
    spacing:          10px ;
}
element {
    children:         [element-icon, element-text]; background-image: linear-gradient(to right, white/10%, grey/30%, black/20%, black/70%);
    border:           3 1 2 1 ;
    border-color:     @background;
    border-radius:    20 ;
    margin:           40 20 10 ;
    padding:          15 px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    background-color: @cream ;
    border:           4px 1px 2px 1px ;
    border-color:     @foreground;
    border-radius:    10% ;
    margin:           17 300 22 ;
    padding:          0 ;
}
