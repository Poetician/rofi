/**
 * Base16 avocado_2 ROFI Color theme
 *
 * Authors
 *  Scheme: avocado 04 Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 *  Customization: Poetician Edition (https://github.com/poetician)
 */

* {
    clear:                       #00000000;
    green1:                      #d2e190;
    green2:                      #ECFBAA;
    green3:                      #234f1e;
    green4:                      #81a140;
    green5:                      #74923a;
    lightfg:                     #81A140;
    lightbg:                     #617b3066;
    foreground:                  #234f1ecc;
    background:                  #A3BF45;
    background-color:            #A3BF4533;
    separator-color:             @clear;
    border-color:                #2d5113;
    selected-normal-foreground:  @green1;
    selected-normal-background:  @green3;
    selected-active-foreground:  @lightfg;
    selected-active-background:  @green2;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @lightfg;
    normal-foreground:           @green4;
    normal-background:           @background;
    active-foreground:           @green1;
    active-background:           @background;
    urgent-foreground:           @lightfg;
    urgent-background:           @background;
    alternate-normal-foreground: @green3;
    alternate-normal-background: @green1;
    alternate-active-foreground: @green5;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @background;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    background-color: @background;
    border:           6;
    border-radius:    12;
    padding:          4;
    width:            50%;
}
mainbox {
    border:           6 ;
    border-radius:    10 ;
    padding:          4;
}
inputbar {
    background-color: @background;
    border:           6 3 6 3 ;
    border-color:     @green4;
    border-radius:    9 ;
    border-spacing:   6;
    margin:           3px ;
    padding:          6px;
    spacing:          6px;
    text-color:       @green1;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    background-color: @clear;
    spacing:          0;
    text-color:       inherit;
}
entry {
    background-color: @clear;
    spacing:          0;
    text-color:       inherit;
}
prompt {
    background-color: @clear;
    spacing:          0;
    text-color:       inherit;
}
textbox-prompt-colon {
    background-color: @clear;
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
}
message {
    background-color: @background;
    border:           6 3 6 3 ;
    border-color:     @green4;
    border-radius:    9 ;
    border-spacing:   6;
    margin:           3px ;
    padding:          6px;
}
textbox {
    text-color:       @foreground;
}
listview {
    columns:          4 ;
    lines:            4 ;
    border:           2px 0px 3px ;
    border-radius:    8 ;
    border-color:     @separator-color;
    spacing:          9px ;
    padding:          4px 2px 4px 2px ;
}
element-icon {
    size:             2.2em;
}
element {
    orientation:      vertical;
}
element-text {
    horizontal-align: 0.45;
    text-color:       inherit;    
}
element {
    border:           4 ;
    border-radius:    12 ;
    padding:          4px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    background-color: @background;
    border:           6 3 6 3 ;
    border-color:     @green4;
    border-radius:    9 ;
    border-spacing:   6;
    margin:           3px ;
}
button {
    spacing:          0;
    text-color:       @normal-foreground;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
