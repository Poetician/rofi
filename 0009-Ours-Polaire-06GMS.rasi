/**
 * Base16 oomox-0009 Ours Polaire ROFI Color theme
 *
 * Authors
 *  Scheme: 0009 Ours Polaire 06GMS Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    clear:                       #00000000;
    silver:                      #D2EAEE;
    black:                       #2F2A2E;
    white:                       #ECEBD7;
    grey:                        #5e606b66;
    red:                         #87632F;
    blue:                        #8FA0B566;
    lightbg:                     #BBC9CB80;
    foreground:                  #3c3e4a33;
    lightfg:                     #d9eff433;
    background:                  #D2EAEE4d;
    background-color:            #ECEBD71a;
    separator-color:              @foreground;
    border-color:                @foreground;
    selected-normal-foreground:  @silver;
    selected-normal-background:  @black;
    selected-active-foreground:  @red;
    selected-active-background:  @black;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @red;
    normal-foreground:           @white;
    normal-background:           @background;
    active-foreground:           @blue;
    active-background:           @background;
    urgent-foreground:           @red;
    urgent-background:           @background;
    alternate-normal-foreground: @white;
    alternate-normal-background: @lightbg;
    alternate-active-foreground: @blue;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @red;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    width:            75% ;
    anchor:           southeast ;
    location:         southeast ;
    background-color: @background;
    border:           5px 2px 5px 2px ;
    border-radius:    24 ;
    padding:          10 ;
    x-offset:         -20px ;
    y-offset:         -75px ;
}
mainbox {
    border:           5px 2px 5px 2px ;
    border-radius:    20 ;
    padding:          10 ;
}
inputbar {
    background-image: linear-gradient(to right, white/50%, grey/50%, black/80%);
    border:           5px 2px 5px 2px ;
    border-color:     @foreground;
    border-spacing:   2 ;
    border-radius:    20%;
    margin:           0px 50px 5px 50px ;
    padding:          5px;
    spacing:          10px;
    text-color:       @black;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    spacing:          0;
    text-color:       @black;
    background-color: @clear;
}
entry {
    spacing:          0;
    text-color:       @black;
    background-color: @clear;
}
prompt {
    spacing:          0;
    text-color:       @black;
    background-color: @clear;
}
textbox-prompt-colon {
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
    background-color: @clear;
}
message {
    border:           5px 2px 5px 2px ;
    border-radius:    10% ;
    border-color:     @separator-color;
    margin:           2 20 5 ;
    padding:          5px ;
}
textbox {
    border:           5px 2px 5px 2px ;
    border-radius:    10% ;
    border-color:     @separator-color;
    margin:           2 5 ;
    padding:          5px ;
    text-color:       @foreground;
}
listview {
    columns:          5;
    lines:            2;
    fixed-height:     0;
    border:           5px 2px 5px 2px ;
    border-radius:    24;
    border-color:     @separator-color;
    spacing:          10px ;
    scrollbar:        false;
    padding:          15px ;
}
element-icon {
    size:             2.7em ;
}
element {
    orientation:      vertical;
}
element-text {
    horizontal-align: 0.5;
    text-color:       inherit;
}
element {
    children: [element-icon, element-text];
    background-image: linear-gradient(to right, white/50%, grey/50%, black/80%);
    border:           5px 2px 5px 2px ;
    border-radius:    20;
    border-color:     @red;
    padding:          6px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    border:           5px 2px 5px 2px ;
    border-radius:    10 ;
    border-color:     @separator-color;
    margin:           10 100 0 ;
    padding:          0 ;
}
