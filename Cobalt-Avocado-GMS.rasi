/**
 * Cobalt-Avocado Color Theme from Named Colors
 *
 * Authors
 *  Scheme: Cobalt-Avocado-GMS Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    clear:                       #00000000;
    silver:                      #0000000d;
    cobalt:                      #223f85;
    mint:                        #d2e190;
    midgreen:                    #617b30;
    darkgreen:                   #234f1e;
    lightfg:                     #b3d28d;
    lightbg:                     #74923a80;
    foreground:                  #001c00;
    background:                  #223f8599;
    background-color:            #223f854d;
    separator-color:             @foreground;
    border-color:                @foreground;
    selected-normal-foreground:  @cobalt;
    selected-normal-background:  @foreground;
    selected-active-foreground:  @midgreen;
    selected-active-background:  @darkgreen;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @midgreen;
    normal-foreground:           @lightfg;
    normal-background:           @cobalt;
    active-foreground:           @darkgreen;
    active-background:           @background;
    urgent-foreground:           @midgreen;
    urgent-background:           @background;
    alternate-normal-foreground: @foreground;
    alternate-normal-background: @lightbg;
    alternate-active-foreground: @darkgreen;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @midgreen;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    background-color: @silver;
    border:           0px 0px 0px;
    border-radius:    0 0 0 0 ;
    margin:           0 5 0 5 ;
    padding:          0;
    width:            65% ;
    x-offset:         0;
    y-offset:         0;
}
mainbox {
    background-image: linear-gradient(to top, #223f85, #b3d28d);
    border:           0px 4px 9px 1px ;
    border-radius:    0 0 15 15 ;
    margin:           0 ;
    padding:          9;
}
inputbar {
    background-image: linear-gradient(to bottom, #223f85, #b3d28d);
    border:           0px 4px 9px 1px ;
    border-color:     @foreground;
    border-radius:    0 0 15 15 ;
    border-spacing:   5px 0 0 0;
    margin:           5px 5px 5px;
    padding:          5px;
    spacing:          10px;
    text-color:       @normal-foreground;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    background-color: @clear;
    spacing:          0;
    text-color:       @normal-foreground;
}
entry {
    background-color: @clear;
    spacing:          0;
    text-color:       @normal-foreground;
}
prompt {
    background-color: @clear;
    spacing:          0;
    text-color:       @normal-foreground;
}
textbox-prompt-colon {
    background-color: @clear;
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
}
message {
    background-image: linear-gradient(to bottom, #223f85, #b3d28d);
    border:           0px 4px 9px 1px ;
    border-color:     @separator-color;
    border-radius:    0 0 15 15 ;
    margin:           0 5 ;
    padding:          9px ;
}
textbox {
    background-image: linear-gradient(to top, #223f85, #b3d28d);
    border:           0px 4px 9px 1px ;
    border-color:     @separator-color;
    border-radius:    0 0 15 15 ;
    padding:          9px ;
    text-color:       @foreground;
}
listview {
    columns:          3;
    lines:            5;
    border:           0px 4px 9px 1px ;
    border-radius:    0 0 15 15 ;
    border-color:     @separator-color;
    margin:           0 5 ;
    padding:          9px ;
    spacing:          9px ;
}
element-icon {
    size:             3em ;
}
element {
    orientation:      horizontal;
}
element-text {
    vertical-align:   0.55;
    text-color:       inherit;    
}
element {
    background-image: linear-gradient(to top, #223f85, #b3d28d);
    border:           0px 4px 9px 1px ;
    border-radius:    0 0 15 15 ;
    padding:          9px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    background-color: #223f85;
    border:           0px 4px 9px 1px ;
    border-color:     @separator-color;
    border-radius:    0 0 15 15 ;
    margin:           6 5 8 5 ;
    padding:          9px ;
}
button {
    spacing:          0;
    text-color:       @normal-foreground;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
