/**
 * Base16 Floating Ovals TanGreen Color theme
 *
 * Authors
 *  Scheme: Floating Ovals TanGreen Theme, Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    brown:                       #6F2C8066;
    black:                       #000000;
    clear:                       #00000000;
    golden:                      #d4ba66;
    tan:                         #b58752;
    text:                        #FBEB7F;
    yellow:                      #faf9e7b3;
    lightfg:                     #FFE9FE99;
    lightbg:                     #905c1a;
    foreground:                  #451b02;
    background:                  @clear;
    background-color:            @clear;
    separator-color:             @foreground;
    border-color:                @yellow;
    selected-normal-foreground:  @black;
    selected-normal-background:  @clear;
    selected-active-foreground:  @black;
    selected-active-background:  @brown;
    selected-urgent-foreground:  @golden;
    selected-urgent-background:  @tan;
    normal-foreground:           @yellow;
    normal-background:           @golden;
    active-foreground:           @brown;
    active-background:           @background;
    urgent-foreground:           @tan;
    urgent-background:           @background;
    alternate-normal-foreground: @lightfg;
    alternate-normal-background: @lightbg;
    alternate-active-foreground: @brown;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @tan;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    background-color: @clear ;
    border:           0;
    border-radius:    20 ;
    margin:           0 ;
    padding:          0;
    width:            80% ;
}
mainbox {
    background-color: @clear;
    border:           0;
    border-radius:    0;
    padding:          0;
}
inputbar {
    background-image: linear-gradient(to right, #000001, #315124, #3f5c26, #4f6728, #607229, #737d2b, #87872d, #9c9130, #b39b35, #cba43c, #e4ac44);
    border:           0 0 25 0 ;
    border-color:     @clear;
    border-radius:    30 ;
    border-spacing:   5 ;
    margin:           15 125 15 125 ;
    padding:          12px 12px 4px 24px ;
    spacing:          20px;
    text-color:       @text ;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    background-color: @clear;
    spacing:          0;
    text-color:       @text;
}
entry {
    background-color: @clear;
    spacing:          0;
    text-color:       @text;
}
prompt {
    background-color: @clear;
    spacing:          0;
    text-color:       @text;
}
textbox-prompt-colon {
    background-color: @clear;
    expand:           false;
    str:              ":";
    text-color:       @text;
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
}
message {
    background-image: linear-gradient(to left, #06402b, #315124, #3f5c26, #4f6728, #607229, #737d2b, #87872d, #9c9130, #b39b35, #cba43c, #e4ac44);
    border:           8 10 12;
    border-radius:    50 ;
    border-color:     @clear;
    margin:           15 55 ;
    padding:          8px 8px 4px 4px ;
}
textbox {
    background-image: linear-gradient(to right, #06402b, #315124, #3f5c26, #4f6728, #607229, #737d2b, #87872d, #9c9130, #b39b35, #cba43c, #e4ac44);
    border:           8 10 12;
    border-radius:    30 ;
    border-color:     @clear;
    margin:           15 ;
    padding:          8px 12px 8px 8px ;
    text-color:       @text;
}
listview {
    columns:          3;
    lines:            3;
    background-color: @clear;
    border:           0;
    border-color:     @separator-color;
    border-radius:    20;
    padding:          20px ;
    spacing:          30px ;
}
element {
    border:           0 ;
    border-radius:    60 ;
    padding:          20 ;
}
element-icon {
    size:             4em ;
}
element {
    orientation:      horizontal;
}
element-text {
    vertical-align:   0.45;
    text-color:       inherit;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    background-image: linear-gradient(to right, #000001, #315124, #3f5c26, #4f6728, #607229, #737d2b, #87872d, #9c9130, #b39b35, #cba43c, #e4ac44);
    border:           8 10 12;
    border-radius:    30 ;
    border-color:     @clear;
    margin:           15 125 ;
    padding:          12px 12px 4px 24px ;
}
button {
    spacing:          0;
    text-color:       @normal-foreground;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
