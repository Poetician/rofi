# usr-share-rofi-themes

The base themes have mostly been generated in Themix. This project started by cloning DT's wallpaper photo collection. All the 0xyz themes were originally designed to fit pixel perfect with the wallpwpers. I now only use my photos manipulated in Gimp and G'mic. All but one or maybe two themes are centered by default. The themes are standalone, since it's unlikely I'll integrate the color palettes with those of other apps, namely Bumblebee-status, Polybar, Alacritty, Jgmenu (if I ever resurrect it), and a few others as the case may be. 

As a historical note, the dash-enabled themes are all gone permanently.

- MS represents mode-switcher, a feature I gravitated to for every theme. It has quirks. Various font icons wouldn't work, and one result is a slight horizontal misalignment in the bar.

- FS represents fullscreen which is self-explanatory.
 
- G represents gradient, for example:

`element {
        children: [element-icon, element-text];
        background-image: linear-gradient(to right, white/30%, grey/50%, black/70%);
}`

- The following lines enable selecting foreground colors, otherwise there is no control I know of.

`element-text {
    text-color:       inherit;
}`


