/**
 * Base16 oomox-0050 Round Suru ROFI Color theme
 *
 * Authors
 *  Scheme: 0163 Swamp WGMS Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    white:                       #f3f1e4;
    clear:                       #00000000;
    text:                        #CECEC6;
    grey:                        #DADADA;
    black:                       #2c2f50;
    red:                         #D04737;
    blue:                        #8b9ebc;
    lightfg:                     #9EA190;
    lightbg:                     #4A5858;
    foreground:                  #384243;
    background:                  #717B73;
    background-color:            #717B734d;
    separator-color:             @foreground;
    border-color:                @background;
    selected-normal-foreground:  @white;
    selected-normal-background:  @lightbg;
    selected-active-foreground:  @red;
    selected-active-background:  @black;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @red;
    normal-foreground:           @text;
    normal-background:           @background;
    active-foreground:           @blue;
    active-background:           @background;
    urgent-foreground:           @red;
    urgent-background:           @background;
    alternate-normal-foreground: @foreground;
    alternate-normal-background: @lightfg;
    alternate-active-foreground: @blue;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @red;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    background-image: linear-gradient(to right, #0B150D, #252F27, #3E4840, #58625A, #717B73, #8B958D, #A4AEA6, #BEC8C0, #D7E1D9);
    anchor:           south;
    location:         south;
    border:           0 ;
    border-radius:    14;
    padding:          0 ;
    width:            70% ;
    x-offset:         0;
    y-offset:         -40;
}
mainbox {
    border:           3px 9px 6px ;
    border-color:     @separator-color;
    border-radius:    10 10 14 14 ;
    margin:           7 14 ;
    padding:          14 7 ;
}
message {
    background-image: linear-gradient(to top, #0B150D, #252F27, #3E4840, #58625A, #717B73, #8B958D, #A4AEA6, #BEC8C0, #D7E1D9);
    border:           3px 9px 6px ;
    border-color:     @lightfg;
    border-radius:    22 ;
    margin:           7 50 ;
    padding:          4 ;
    text-color:       @text ;
}
textbox {
    border:           3px 9px 6px ;
    border-color:     @lightfg;
    border-radius:    16 ;
    margin:           0 ;
    padding:          4 6 ;
    text-color:       @foreground;
}
listview {
    columns:          3;
    lines:            6;
    fixed-height:     0;
    border:           6px 12px ;
    border-radius:    24 ;
    border-color:     @background;
    spacing:          20px ;
    scrollbar:        false;
    padding:          4px 8px ;
}
element-icon {
    size:             2.5em ;
}
element-text{
    vertical-align:   0.45;
    text-color:       inherit;
}
element {
    orientation:      horizontal;
}
element {
    border:           6px 12px 9px ;
    border-radius:    24 ;
    padding:          12px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    background-image: linear-gradient(to top, #0B150D, #252F27, #3E4840, #58625A, #717B73, #8B958D, #A4AEA6, #BEC8C0, #D7E1D9);
    border:           4px 6px 8px ;
    border-radius:    30;
    border-color:     @lightfg;
    margin:           5px 75px 10px ;
    padding:          4px 8px 6px ;
    spacing:          10 ;
}
button {
    spacing:          0;
    text-color:       @normal-foreground;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
inputbar {
    background-image: linear-gradient(to top, #0B150D, #252F27, #3E4840, #58625A, #717B73, #8B958D, #A4AEA6, #BEC8C0, #D7E1D9);
    border:           4px 6px 8px ;
    border-color:     @lightfg;
    border-radius:    24 ;
    margin:           5px 100px 10px ;
    padding:          4px 8px 6px ;
    text-color:       @white;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    background-color: @clear;
    spacing:          0;
    text-color:       @white;
}
entry {
    background-color: @clear;
    spacing:          0;
    text-color:       @white;
}
prompt {
    background-color: @clear;
    spacing:          0;
    text-color:       @white;
}
textbox-prompt-colon {
    background-color: @clear;
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       @white;
}
