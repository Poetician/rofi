/**
 * Base16 oomox-0023 Montane ROFI Color theme
 *
 * Authors
 *  Scheme: 0023 Montane 02GMS Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    clear:                       #00000000;
    yellow:                      #F9C45B99;
    lightblue:                   #94C1AA;
    red:                         #B4904980;
    blue:                        #7EB1AE80;
    lightfg:                     #ABBEBD;
    lightbg:                     #0202021a;
    foreground:                  #E2E2D9;
    background:                  #2E2E2A99;
    background-color:            #2E2E2A33;
    separator-color:              @foreground;
    border-color:                @foreground;
    selected-normal-foreground:  @foreground;
    selected-normal-background:  @red;
    selected-active-foreground:  @background;
    selected-active-background:  @blue;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @red;
    normal-foreground:           @foreground;
    normal-background:           @background;
    active-foreground:           @blue;
    active-background:           @background;
    urgent-foreground:           @red;
    urgent-background:           @background;
    alternate-normal-foreground: @foreground;
    alternate-normal-background: @blue;
    alternate-active-foreground: @blue;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @red;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    anchor:           north;
    location:         north;
    width:            60%;
    background-color: @background;
    border:           5 7 9 7 ;
    border-radius:    20;
    padding:          10;
    x-offset:         0;
    y-offset:         40;
}
mainbox {
    border:           5 7 9 7 ;
    border-radius:    12;
    margin:           10 20 10 ;
    padding:          10;
}
message {
    border:           6px 0px 3px 0px ;
    border-color:     @separator-color;
    border-radius:    10% ;
    margin:           10 75 10 ;
    padding:          1px ;
}
textbox {
    border:           6px 0px 3px 0px ;
    border-color:     @separator-color;
    border-radius:    10% ;
    margin:           2px ;
    padding:          4px ;
    text-color:       @foreground;
}
listview {
    columns:          3;
    lines:            7;
    fixed-height:     0;
    border:           6px 0px 0px 0px ;
    border-radius:    20;
    border-color:     @separator-color;
    spacing:          20px ;
    scrollbar:        false;
    padding:          6px 1px 0px 1px ;
}
element-icon {
    size:             2em ;
}
element {
    orientation:      horizontal;
}
element-text {
    vertical-align:   0.45;
    text-color:       inherit;
}
element {
    children: [element-icon, element-text];
    background-image: linear-gradient(to bottom, white/30%, black/40%, black/80%);
    border:           3 3 6 3 ;
    border-color:     @lightblue;
    border-radius:    20;
    padding:          10px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    border:           6px 0px 4px 0px ;
    border-color:     @separator-color;
    border-radius:    10% ;
    margin:           10 75 4 ;
    padding:          4px ;
}
button {
    spacing:          0;
    text-color:       @normal-foreground;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
inputbar {
    background-color: @clear;
    border:           4px 0px 6px 0px ;
    border-color:     @normal-foreground;
    border-radius:    10% ;
    border-spacing:   4;
    margin:           0px 220px 7px 240px ;
    padding:          4 6 4 6 ;
    text-color:       @normal-foreground;
    children:
    [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    spacing:          0;
    text-color:       @normal-foreground;
}
entry {
    spacing:          0;
    text-color:       @normal-foreground;
}
prompt {
    spacing:          0;
    text-color:       @normal-foreground;
}
textbox-prompt-colon {
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
}
