/**
 * Base16 oomox-0009 Ours Polaire ROFI Color theme
 *
 * Authors
 *  Scheme: 0009 Ours Polaire 1L5MS Poetician Edition (https://gitlab.com/Poetician)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    white:                       #d1cff4;
    clear:                       #00000000;
    text:                        #d9eff4;
    grey:                        #3c3e4a;
    dark:                        #704B15;
    lightfg:                     #d9eff480;
    lightbg:                     @clear;
    foreground:                  @white;
    background:                  @clear;
    background-color:            @clear;
    separator-color:             @foreground;
    border-color:                @foreground;
    selected-normal-foreground:  @grey;
    selected-normal-background:  @lightfg;
    selected-active-foreground:  @text;
    selected-active-background:  @grey;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @text;
    normal-foreground:           @text;
    normal-background:           @background;
    active-foreground:           @grey;
    active-background:           @background;
    urgent-foreground:           @text;
    urgent-background:           @background;
    alternate-normal-foreground: @dark;
    alternate-normal-background: @foreground;
    alternate-active-foreground: @grey;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @text;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    anchor:           east;
    location:         east;
    background-color: @background;
    border-radius:    15;
    border:           1;
    padding:          2;
    width:            40%;
    x-offset:         -5px;
    y-offset:         -5px;
}
mainbox {
    border:           0;
    border-radius:    12;
    padding:          16;
}
message {
    border:           1px 0px 0px 0px ;
    border-radius:    15;
    border-color:     @white;
    margin:           4 40 4 ;
    padding:          1px ;
}
textbox {
    border:           1px 0px 0px 0px ;
    border-radius:    15;
    border-color:     @white;
    margin:           4 40 4 ;
    padding:          1px ;
    text-color:       @foreground;
}
element-icon {
    size:             2.2em ;
}
element {
    orientation:      vertical;
}
element-text {
    horizontal-align: 0.5;
    text-color:       inherit;
}
listview {
    lines:            7;
    columns:          2;
    border:           0px ;
    border-radius:    18px;
    border-color:     @white;
    spacing:          24px ;
    scrollbar:        false;
    padding:          4px ;
}
element {
    children: [element-icon, element-text];
    background-image: linear-gradient(to left, grey/40%, white/10%, black/15%);
    border:           1;
    border-radius:    15;
    padding:          4px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    border:           1px 0px 0px 0px ;
    border-radius:    15;
    border-color:     @white;
    margin:           4 ;
    padding:          1px ;
}
button {
    spacing:          0;
    text-color:       @normal-foreground;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
inputbar {
    backgound-color:  @clear;
    border:           1;
    border-color:     @foreground;
    border-radius:    9;
    margin:           2px 8px 12px 8px ;
    padding:          0px 18px 12px 18px ;
    spacing:          8px;
    text-color:       @text;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    backgound-color:  @clear;
    spacing:          0;
    text-color:       inherit;
}
entry {
    backgound-color:  @clear;
    spacing:          0;
    text-color:       inherit;
}
prompt {
    backgound-color:  @clear;
    spacing:          0;
    text-color:       inherit;
}
textbox-prompt-colon {
    backgound-color:  @clear;
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
}
