/**
 * Base16 oomox-0129 Autumn Windbreak ROFI Color theme
 *
 * Authors
 *  Scheme: 0129 Autumn Windbreak 02GMS Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    clear:                       #00000000;
    grey:                        #AFB0B5;
    black:                       #22282d99;
    red:                         #D04737;
    blue:                        #9499a980;
    lightfg:                     #E2E3E8;
    lightbg:                     #ECEDF2;
    foreground:                  #31334080;
    background:                  #B7A49080;
    background-color:            #B7A49000;
    separatorcolor:              @foreground;
    border-color:                @foreground;
    selected-normal-foreground:  @black;
    selected-normal-background:  @lightbg;
    selected-active-foreground:  @background;
    selected-active-background:  @blue;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @red;
    normal-foreground:           @lightfg;
    normal-background:           @background;
    active-foreground:           @blue;
    active-background:           @background;
    urgent-foreground:           @red;
    urgent-background:           @background;
    alternate-normal-foreground: @black;
    alternate-normal-background: @grey;
    alternate-active-foreground: @blue;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @red;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    width:            50%;
    background-image: linear-gradient(to right, white/30%, grey/50%, black/70%);
    border:           5 10 5 2;
    border-radius:    6 18 12 9;
    padding:          5;
}
mainbox {
    border:           5 10 5 2;
    border-radius:    5 20 20 10;
    border-color:     @separatorcolor;
    padding:          5px ;
}
inputbar {
    background-image: linear-gradient(to right, white/30%, grey/50%, black/70%);
    padding:          5 5 5 5px ;
    border-spacing:   0 0 0 0;
    border:           5 10 5 2;
    border-radius:    5 20 20 10;
    spacing:          6px;
    margin:           5px 5px 5px 5px ;
    border-color:     @foreground;
    text-color:       @lightbg;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    background-color: @clear;
    spacing:          0;
    text-color:       @lightbg;
}
entry {
    background-color: @clear;
    spacing:          0;
    text-color:       @lightbg;
}
prompt {
    background-color: @clear;
    spacing:          0;
    text-color:       @lightbg;
}
textbox-prompt-colon {
    background-color: @clear;
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
}
message {
    border:           5 20 5 2;
    border-radius:    5 20 20 10;
    border-color:     @separatorcolor;
    margin:           5px ;
    padding:          5px ;
}
textbox {
    border:           5 10 5 2;
    border-radius:    5 20 20 10;
    border-color:     @separatorcolor;
    margin:           5px 15px 5px ;
    padding:          5 ;
    text-color:       @lightbg;
}
listview {
    columns:          3;
    lines:            6;
    border-radius:    6 18 12 9;
    border-color:     @separatorcolor;
    spacing:          12px ;
    scrollbar:        false;
    padding:          5 5 5 5 ;
}
element-icon {
    size:             2.5em ;
}
element-text {
    vertical-align:   0.5;
    text-color:       inherit;
}
element {
    orientation:      horizontal;
}
element {
    children: [element-icon, element-text];
    background-image: linear-gradient(to right, white/30%, grey/50%, black/70%);
    border:           5 10 5 2;
    border-radius:    5 20 20 10;
    padding:          5 0 5 5 ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    border:           5 10 5 2;
    border-radius:    5 20 20 10;
    border-color:     @separatorcolor;
    margin:           5px 25px ;
    padding:          4px 2px ;
}
