/**
 * Base16 oomox-Ail ROFI Color theme
 *
 * Authors
 *  Scheme: Ail-02 Poetician Edition (https://gitlab.com/Poetician/rofi)
 *  Template: Jordi Pakey-Rodriguez (https://github.com/0xdec), Andrea Scarpino (https://github.com/ilpianista)
 */

* {
    black:                       #300000;
    clear:                       #00000000;
    red:                         #12020299;
    blue:                        #6a5d6699;
    lightfg:                     #6a5d6633;
    lightbg:                     #28282866;
    foreground:                  #373356b3;
    background:                  #bcb5b1;
    background-color:            #bcb5b133;
    separatorcolor:              @foreground;
    border-color:                @foreground;
    selected-normal-foreground:  @black;
    selected-normal-background:  @lightfg;
    selected-active-foreground:  @background;
    selected-active-background:  @blue;
    selected-urgent-foreground:  @background;
    selected-urgent-background:  @red;
    normal-foreground:           @foreground;
    normal-background:           @background;
    active-foreground:           @blue;
    active-background:           @background;
    urgent-foreground:           @red;
    urgent-background:           @background;
    alternate-normal-foreground: @foreground;
    alternate-normal-background: @lightbg;
    alternate-active-foreground: @blue;
    alternate-active-background: @lightbg;
    alternate-urgent-foreground: @red;
    alternate-urgent-background: @lightbg;
    spacing:                     2;
}
window {
    background-image: linear-gradient(to bottom, #bcb5b1, #6a5d6633, #120202, #282828) ;
    border:           7 ;
    border-radius:    14 ;
    padding:          7 ;
    width:            50% ;
}
mainbox {
    background-color: @background;
    border:           7 ;
    border-radius:    10;
    padding:          7 ;
}
inputbar {
    background-image: linear-gradient(to right, #bcb5b1, #6a5d66, #282828);
    border:           2 3 2 3 ;
    border-color:     @foreground;
    border-radius:    10 ;
    border-spacing:   0 ;
    margin:           0px 200 8px 200;
    padding:          4 ;
    spacing:          0 0 0 0 ;
    text-color:       @normal-foreground;
    children:         [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
case-indicator {
    background-color: @clear;
    spacing:          0;
    text-color:       @normal-foreground;
}
entry {
    background-color: @clear;
    spacing:          0;
    text-color:       @normal-foreground;
}
prompt {
    background-color: @clear;
    spacing:          0;
    text-color:       @normal-foreground;
}
textbox-prompt-colon {
    background-color: @clear;
    expand:           false;
    str:              ":";
    margin:           0px 0.3000em 0.0000em 0.0000em ;
    text-color:       inherit;
}
message {
    border:           2px 0px 0px 0px ;
    border-radius:    10;
    border-color:     @separatorcolor;
    padding:          7px ;
}
textbox {
    text-color:       @foreground;
    border:           5px;
    border-radius:    10px;
}
listview {
    columns:          2;
    lines:            5;
    border:           3px 1px 2px ;
    border-radius:    10;
    border-color:     @separatorcolor;
    spacing:          3px ;
    scrollbar:        false;
    padding:          3px 0px 1px 0px ;
}
element-icon {
    size:             2.2em ;
}
element {
    orientation:      horizontal;
}
element-text {
    vertical-align:   0.45 ;
    text-color:       inherit ;    
}
element {
    border:           4 ;
    border-radius:    10;
    padding:          5px ;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
mode-switcher {
    border:           2 3 2 3 ;
    border-color:     @foreground;
    border-radius:    10 ;
    border-spacing:   0 ;
    margin:           4px 2 4px 2 ;
    padding:          0 ;
}
button {
    spacing:          0;
    text-color:       @normal-foreground;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
